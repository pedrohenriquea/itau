package com.itau.comunicador.model;

public class EmailRequest {

	private String email;

	private String template;

	private String subject;

	public EmailRequest() {
	}

	public EmailRequest(String email, String template, String subject) {
		super();
		this.email = email;
		this.template = template;
		this.subject = subject;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

}
