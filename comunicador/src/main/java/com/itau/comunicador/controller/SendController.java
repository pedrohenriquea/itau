package com.itau.comunicador.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.comunicador.model.SmsRequest;
import com.itau.comunicador.service.EmailService;
import com.itau.comunicador.service.SmsService;


@RestController
@RequestMapping("/send")
public class SendController{

	@Autowired
	private SmsService smsService;

	@Autowired
	private EmailService emailService;

	@PostMapping
	public ResponseEntity<Void> sendSMS( @Valid @RequestBody SmsRequest smsRequest) {

		smsService.sendSingleSMS(smsRequest.getMessage(), smsRequest.getPhoneNumber());

		return new ResponseEntity<Void>(HttpStatus.OK);
	}

//	@PostMapping(value = "/send/email")
//	public ResponseEntity<Void> sendEmail(@Valid @RequestBody EmailRequest emailRequest) {
//
//		emailService.sendEmail(emailRequest.getTemplate(), emailRequest.getSubject(), emailRequest.getEmail());
//
//		return new ResponseEntity<Void>(HttpStatus.OK);
//	}

}
