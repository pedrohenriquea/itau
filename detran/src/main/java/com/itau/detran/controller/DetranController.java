package com.itau.detran.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.itau.detran.api.DetranApi;
import com.itau.detran.api.model.TributoModel;
import com.itau.detran.model.PayloadSQS;
import com.itau.detran.service.ProdespService;
import com.itau.detran.service.RelatorioService;

@RestController
public class DetranController implements DetranApi {

	@Autowired
	private ProdespService prodespService;
	
	@Autowired
	private RelatorioService relatorioService;
	
	@Override
	public ResponseEntity<List<TributoModel>> getTributosPendentesPagamento(String cnpj, String tipoTributo) {
		return ResponseEntity.ok().body(prodespService.getTributosPendentesPagamento(cnpj, tipoTributo));
	}
	
	@Override
	public ResponseEntity<Void> postRealizarPagamentoTributos(String cpf, List<TributoModel> tributoModel) {
		
		prodespService.pagarTributosSelecionados(tributoModel, cpf);
		
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<Void> postSolicitarRelatorio(String cpf) {
		
		relatorioService.putMessageQueue(cpf);
		
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@SqsListener("relatorios-solicitados")
	public void listenerMensagemRelatorioSolicitado(PayloadSQS message) {
		relatorioService.gerarRelatorio(message.getPayload());
		
	}
}
