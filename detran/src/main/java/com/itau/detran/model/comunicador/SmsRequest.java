package com.itau.detran.model.comunicador;

public class SmsRequest {

	private String message;

	private String phoneNumber;

	public SmsRequest() {
	}

	public SmsRequest(String message, String phoneNumber) {
		super();
		this.message = message;
		this.phoneNumber = phoneNumber;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}
