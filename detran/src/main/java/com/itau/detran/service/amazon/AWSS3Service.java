package com.itau.detran.service.amazon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

@Service
public class AWSS3Service {
	
	@Autowired
	private S3Client s3Client;
	
	public void uploadFile(String key, byte[] bytes) {
		
		
		s3Client.putObject(PutObjectRequest.builder().bucket("relatorios-detran").key(key).build(), RequestBody.fromBytes(bytes));
		
		System.out.println("Arquivo enviado! " + key);
	}


}
