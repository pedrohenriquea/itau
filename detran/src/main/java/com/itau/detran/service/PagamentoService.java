package com.itau.detran.service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse.BodyHandlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

@Service
public class PagamentoService {
	
	@Autowired
	private HttpClient httpClient;
	
	public void realizarPagamento(String cpf, Double valorPagamento) {
		
		try {
			
			var request = HttpRequest.newBuilder()
					.uri(URI.create("https://my-json-server.typicode.com/pedrohenriquea/prodesp-fake-api/pagamentos"))
					.header("Content-Type", MediaType.APPLICATION_JSON_VALUE)
					.header("cpf", cpf)
					.header("valor-pagamento", valorPagamento.toString())
					.method(HttpMethod.POST.toString(), BodyPublishers.ofString(""))
					.build();
			
			var response = httpClient.send(request, BodyHandlers.ofString());
			
			if (response.statusCode() != HttpStatus.OK.value())
				throw new Exception("");
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void realizarEstornoPagamento(String cpf, Double valorEstorno) {
		try {

			var request = HttpRequest.newBuilder()
					.uri(URI.create("https://my-json-server.typicode.com/pedrohenriquea/prodesp-fake-api/estornos"))
					.header("Content-Type", MediaType.APPLICATION_JSON_VALUE).header("cpf", cpf)
					.header("valor-estorno", valorEstorno.toString())
					.method(HttpMethod.POST.toString(), BodyPublishers.ofString(""))
					.build();

			var response = httpClient.send(request, BodyHandlers.ofString());

			if (response.statusCode() != HttpStatus.OK.value())
				throw new Exception("");

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
